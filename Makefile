NASM := nasm -f elf64 -o
PY=python3

program: main.o lib.o dict.o
	ld -o $@ $^
main.o: main.asm lib.o dict.o words.inc colon.inc
	$(NASM) $@ $<
dict.o: dict.asm lib.o
	$(NASM) $@ $<
%.o: %.asm words.inc colon.inc dict.inc lib.inc
  $(NASM) $@ $<
.PHONY: clean
clean:
	rm *.o program
	
.PHONY: test
test: program
	$(PY) test.py

	

