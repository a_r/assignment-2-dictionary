section .text

global find_word

%include 'lib.inc'
%define OFFSET 8;

find_word:
   push rsi
   push rdi
   mov r12, rsi
   mov r13, rdi
   .loop:          
       add r12, OFFSET      
       call string_equals         
       test rax, rax       
       jnz .found          
       mov r12, [r12]      
       test r12, r12
       jnz .loop          
       xor rax, rax        
       ret   
   .found:
       mov rax, r12
       pop rdi             
       pop rsi         
       ret                  

