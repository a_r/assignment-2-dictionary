from subprocess import Popen, PIPE

inputs = ["key1", "key2", "key3", "f"*256, "f"*255, "dksjahdjkhaskjd"]
outputs = ["Wake up Neo", "Assembly", "has you", "", "", "Word was not found"]
errors = ["", "", "", "Key is too long", "Key is too long", ""]

for i in range(len(inputs)):
    process = Popen(["./program"], 
		    stdin=PIPE, 
		    stdout=PIPE, 
		    stderr=PIPE)
    data_inputs = process.communicate(inputs[i].encode())
    
    if data_inputs[0].decode().strip() == outputs[i]:
        print(f"OK stdout for {inputs}: {outputs}")
    else:
        print(f"Not OK stdout for {inputs}: {data_inputs[0].decode().strip()}, expected {outputs}")
        
    if data_inputs[1].decode().strip() == errors[i]:
        print(f"OK stderr for {inputs}: {errors}")
    else:
        print(f"Not OK stderr for {inputs}: {data_inputs[1].decode().strip()}, expected {errors}") 
        
        
        

   
   
   
   

