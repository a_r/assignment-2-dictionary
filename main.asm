%include "words.inc"


%define BUFFER_SIZE 256
%define STDOUT 1
%define STDERR 2
%define OFFSET 8

section .bss
buffer: resb BUFFER_SIZE

section .rodata
too_long_msg: db "Key is too long", 0
not_found_msg: db "Word was not found", 0

section .text
%include "dict.inc"
%include "lib.inc"

global _start

_start:
	mov rdi, buffer
	mov rsi, BUFFER_SIZE
	call read_word
	test rax, rax
	jz .too_long
	mov rdi, buffer
	mov rsi, next_el 
	call find_word
	test rax, rax
	jz .not_found
	lea rdi, [rax+OFFSET]
	call string_length
	lea rdi, [rdi+rax+1]
	call print_string
	jmp exit
.too_long:
	mov rdi, too_long_msg
	call print_error
	mov rdx, rax				
	mov rax, STDOUT			
	mov rdi, STDERR				
	mov rsi, too_long_msg
	syscall
	jmp exit
.not_found:
	mov rdi, not_found_msg
	call print_error
	jmp exit

